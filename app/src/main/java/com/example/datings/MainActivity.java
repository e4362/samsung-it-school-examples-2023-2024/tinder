package com.example.datings;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        FragmentTransaction ft2 = fm.beginTransaction();

        ButtonsFragment bf = new ButtonsFragment();
        PhotoFragment pf = new PhotoFragment();

        ft.replace(R.id.frameLayout1, bf);
        ft.commit();

        ft2.replace(R.id.frameLayout2, pf);
        ft2.commit();

    }
}